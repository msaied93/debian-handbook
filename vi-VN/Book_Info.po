# TxGVNN <txgvnn@gmail.com> 2017
msgid ""
msgstr ""
"Project-Id-Version: 0\n"
"POT-Creation-Date: 2022-08-10 10:50+0200\n"
"PO-Revision-Date: 2021-03-08 23:02+0000\n"
"Last-Translator: bruh <quangtrung02hn16@gmail.com>\n"
"Language-Team: Vietnamese <https://hosted.weblate.org/projects/debian-handbook/book_info/vi/>\n"
"Language: vi-VN\n"
"MIME-Version: 1.0\n"
"Content-Type: application/x-publican; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 4.5.1\n"

msgid "The Debian Administrator's Handbook"
msgstr "Sổ tay Quản trị Debian"

#, fuzzy
#| msgid "Debian Buster from Discovery to Mastery"
msgid "Debian Bullseye from Discovery to Mastery"
msgstr "Debian Buster từ Khám phá đến Làm chủ"

msgid "Debian"
msgstr "Debian"

msgid "A reference book presenting the Debian distribution, from initial installation to configuration of services."
msgstr "Một cuốn sách tham khảo trình bày bản phân phối Debian, từ cài đặt ban đầu đến cấu hình các dịch vụ."

#, fuzzy
#| msgid "ISBN: 979-10-91414-19-7 (English paperback)"
msgid "ISBN: 979-10-91414-21-0 (English paperback)"
msgstr "ISBN: 979-10-91414-19-7 (Bản bìa mềm tiếng Anh)"

#, fuzzy
#| msgid "ISBN: 979-10-91414-20-3 (English ebook)"
msgid "ISBN: 979-10-91414-22-7 (English ebook)"
msgstr "ISBN: 979-10-91414-20-3 (Sách điện tử tiếng Anh)"

msgid "This book is available under the terms of two licenses compatible with the Debian Free Software Guidelines."
msgstr "Cuốn sách này có sẵn theo các điều khoản của hai giấy phép tương thích với Hướng dẫn Phần mềm Tự do Debian."

msgid "Creative Commons License Notice:"
msgstr "Thông báo Giấy phép Creative Commons:"

msgid "This book is licensed under a Creative Commons Attribution-ShareAlike 3.0 Unported License. <ulink type=\"block\" url=\"https://creativecommons.org/licenses/by-sa/3.0/\" />"
msgstr "Quyển sách này được cấp phép theo giấy phép Creative Commons Attribution-ShareAlike 3.0 Unported. <ulink type=\"block\" url=\"https://creativecommons.org/licenses/by-sa/3.0/\" />"

msgid "GNU General Public License Notice:"
msgstr "Thông báo Giấy phép GNU General Public:"

msgid "This book is free documentation: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 2 of the License, or (at your option) any later version."
msgstr "Quyển sách này là tài liệu tự do: bạn có thể tái phân phối nó và/hoặc chỉnh sửa nó dưới điều khoản của giấy phép GNU General Public như một người phát hành bởi tổ chức Free Software Foundation, hoặc là phiên bản thứ 2 của giấy phép, hoặc (tùy sự lựa chọn của bạn) bất kỳ phiên bản mới nhất nào."

msgid "This book is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details."
msgstr "Quyển sách này phân phối với hi vong nó sẽ hữu ích, nhưng KHÔNG CÓ BẤT KỲ ĐẢM BẢO NÀO; thậm chí không có sự đảm bảo ngụ ý của MERCHANTABILITY hoặc FITNESS FOR A PARTICULAR PURPOSE. Xem Giấy phép GNU General Public để biết thêm chi tiết."

msgid "You should have received a copy of the GNU General Public License along with this program. If not, see <ulink url=\"https://www.gnu.org/licenses/\" />."
msgstr "Bạn có khả năng là đã nhận một bản sao của Giấy phép GNU General Public cùng với chương trình này. Nếu không, hãy xem <ulink url=\"https://www.gnu.org/licenses/\" />."

msgid "Show your appreciation"
msgstr "Thể hiện sự cảm kích của bạn"

msgid "This book is published under a free license because we want everybody to benefit from it. That said maintaining it takes time and lots of effort, and we appreciate being thanked for this. If you find this book valuable, please consider contributing to its continued maintenance either by buying a paperback copy or by making a donation through the book's official website: <ulink type=\"block\" url=\"https://debian-handbook.info\" />"
msgstr "Quyển sách này được xuất bản dưới một giấy phép tự do bởi vì chúng tôi muốn tất cả mọi người hưởng lợi ích từ nó. Điều đó đã nói xong, chúng tôi cũng muốn nói rằng việc duy trì nó tốn thời gian và nhiều nỗ lực, và chúng tôi đánh giá cao việc được cảm ơn vì điều này. Nếu bạn thấy cuốn sách này có giá trị, hãy xem xét đóng góp vào việc tiếp tục duy trì nó bằng cách mua một bản sao bìa mềm hoặc bằng việc quyên góp qua website chính thức của sách: <ulink type=\"block\" url=\"https://debian-handbook.info\" />"
